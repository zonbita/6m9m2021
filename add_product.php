<?php
require_once("product.class.php");
require_once("category.class.php");
	if(isset($_POST["btnsubmit"])){
		$productName = $_POST["txtName"];
		$cateID = $_POST["txtCateID"];
		$price = $_POST["txtprice"];
		$quantity = $_POST["txtquantity"];
		$description = $_POST["txtdesc"];
		$picture = $_FILES["txtpic"];

		//khởi tạo đối tượng product
		$newProduct = new Product($productName, 1, $price, $quantity, $description, $picture);
		//$newProduct = new Product("i phone 6",1, 2, 2, "iphone 6", "iphone 6");
		//Lưu xuống CSDL
		$result = $newProduct->save();
		if(!$result)
		{
			header("Location: add_product.php?failure");

		}
		else{
			header("Location: add_product.php?inserted");
		}
	}

 ?>
<?php include_once("header.php");?>

 <?php
 	if(isset($_GET["inserted"]))
	{

		echo "<h2>Thêm sản phẩm thành công</h2>";
	}
 ?>
<center>
 <form method="post" enctype="multipart/form-data">
	 <div class="row">
		 	<div class="lbltitle">
		 	<label>Tên sản phẩm</label>
	 		</div>
		 <div class="lblinput">
			<input type="text" name="txtName" value="<?php echo isset($_POST["txtName"]) ? $_POST["txtName"] : "";  ?>">
		 </div>
	 </div>

	 <div class="row">
			<div class="lbltitle">
		 <label>Mô tả sản phẩm</label>
	 	</div>
		 <div class="lblinput">
			 <textarea name="txtdesc" cols="21" rows="1" value="<?php echo isset($_POST["txtdesc"]) ? $_POST["txtdesc"]:"";?>"></textarea>
		 </div>
	 </div>

	 <div class="row">
			<div class="lbltitle">
		 <label>Số lượng sản phẩm</label>
		</div>
		 <div class="lblinput">
			 <textarea name="txtquantity" cols="21" rows="1" value="<?php echo isset($_POST["txtquantity"]) ? $_POST["txtquantity"]:"";?>"></textarea>
		 </div>
	 </div>

	 <div class="row">
			<div class="lbltitle">
		 <label>Giá sản phẩm</label>
		</div>
		 <div class="lblinput">
			 <input type="number" name="txtprice" cols="21" rows="1" value="<?php echo isset($_POST["txtprice"]) ? $_POST["txtprice"]:"";?>">
		 </div>
	 </div>

	 <div class="row">
			<div class="lbltitle">
		 <label>Loại sản phẩm</label>
		</div>
		 <div class="lblinput">
			 <select name="txtCateID">
			 	<option value="" selected>--Chọn loại--</option>
				 <?php
				 		$cates = Category::list_category();
						foreach ($cates as $item) {
							echo "<option value=".$item["CateID"].">".$item["CategoryName"]."</option>";
						}
				  ?>
				</select>
		 </div>
	 </div>

	 <div class="row">
			<div class="lbltitle">
		 <label>Hình sản phẩm</label>
		</div>
		 <div class="lblinput">
			 <input type="file" id="txtpic" name="txtpic" accept=".PNG,.GIF,.JPG">

		 </div>
	 </div>

	 <div class="row">
			<div class="submit">
			 <input type="submit" name="btnsubmit" value="Thêm sản phẩm"/>
		</div>

	 </div>

</form>
</center>
<?php include_once("footer.php");?>
